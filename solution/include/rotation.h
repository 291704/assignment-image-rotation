#ifndef ROTATION_H
#define ROTATION_H

#include <stdbool.h>

#include "image.h"

bool img_rotate(const struct image *src, struct image *dst);

#endif
