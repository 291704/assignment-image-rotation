#ifndef IMAGE_TRANS
#define IMAGE_TRANS

#include <inttypes.h>
#include <stdbool.h>

struct __attribute__((packed)) pixel { uint8_t b, g, r; };

struct image {
  uint64_t width, height;
  struct pixel* data;
};

struct image image_init(uint64_t width, uint64_t height);

void image_destroy(struct image* img);

struct pixel get_pixel (const struct image* img, uint64_t x, uint64_t y);

void set_pixel (struct image* img, const struct pixel pixel, uint64_t x, uint64_t y);

#endif
