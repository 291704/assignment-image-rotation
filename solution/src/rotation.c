#include "rotation.h"

#include <inttypes.h>
#include <stdbool.h>

#include "image.h"

bool img_rotate(const struct image *src, struct image *dst) {
    if (!src || !dst) {
        return false;
    }
    *dst = image_init(src->height, src->width);
    if ( !dst->data ) { // если память не выделилась - возвращаем код ошибки -1
        return false;
    }
    for( uint64_t i = 0; i < src->height; i++ ) {
        for( uint64_t j = 0; j < src->width; j++ ) {
            set_pixel(dst, get_pixel(src, j, src->height - i - 1), i, j);
        }
    }
    return true;
}
