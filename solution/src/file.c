#include "file.h"

#include <stdbool.h>
#include <stdio.h>

bool file_open(FILE** file, char* path, char* mode) {
    if (!file || !path || !mode) {
        return false;
    }
    *file = fopen(path, mode);
    if (!*file) {
        return false;
    }
    return true;
}

bool file_close(FILE* file) {
    if (!file) {
        return false;
    }
    if (!fclose(file)) {
        return false;
    }
    return true;
}
