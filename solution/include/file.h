#ifndef FILE_H
#define FILE_H

#include <stdbool.h>
#include <stdio.h>

bool file_open(FILE** file, char* path, char* mode);

bool file_close(FILE* file);

#endif
