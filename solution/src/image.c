#include "image.h"

#include <inttypes.h>
#include <malloc.h>
#include <stdbool.h>

struct image image_init(uint64_t width, uint64_t height) {
    return (struct image) {
        .width = width,
        .height = height,
        .data = malloc(width * height * sizeof (struct pixel))
    };
}

void image_destroy(struct image* img) {
    free(img->data);
    img = NULL;
}

struct pixel get_pixel (const struct image* img, uint64_t x, uint64_t y) {
    return img->data[y * img->width + x];
}

void set_pixel (struct image* img, const struct pixel pixel, uint64_t x, uint64_t y) {
    img->data[y * img->width + x] = pixel;
}
