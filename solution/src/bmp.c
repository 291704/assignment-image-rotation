#include "bmp.h"

#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>

#include "image.h"

#define BF_TYPE 0x4D42
#define BF_RESERVED 0
#define B_OFF_BITS 54
#define BI_SIZE 40
#define BI_PLANES 1
#define BI_BIT_COUNT 24
#define BI_COMPRESSION 0

// структура заголовка bmp файла
struct __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static bool validate_header (const struct bmp_header* const header) {
    return header->bfType == BF_TYPE;
}

static bool read_header (FILE* in, struct bmp_header* header) {
    if(!fread( header, sizeof( struct bmp_header ), 1, in )) { // читаем заголовок bmp, fread возвращает количество прочитанных байтов, если оно равно 0, то ошибка (про fread вам лучше ознакомиться самому)
        return false;
    }
    if(!validate_header(header)) {                             //проверка типа bmp-файла (должен быть BM)
        return false;
    }
    return true;
}

static int64_t calculate_padding (uint64_t width) {
    if (width % 4) {
        return (int64_t) (4 - (3 * width % 4));
    }
    return 0;
}

static bool skip_padding(FILE* in, uint64_t width) {
    if (width % 4) {
        if (fseek(in, calculate_padding(width), SEEK_CUR)) {
            return false;
        }
    }
    return true;
}

static bool read_pixels (FILE* in, struct image* img) {
    for (uint64_t y = 0; y < img->height; y++) {
        if( !fread( img->data + img->width * y, sizeof(struct pixel) * img->width, 1, in )) {
            image_destroy(img);
            return false;
        }   // массив-буфер для выравнивания, он нужен только для fread
        if (!skip_padding(in, img->width)) {
            return false;
        }
    }
    return true;
}

// читаем исходный файл и заполняем структуру исходного изображения
enum read_status from_bmp( FILE* in, struct image* img ) {
    if (!in) {
        return READ_BAD_FILE;
    }

    struct bmp_header header = {0};
    if(!read_header(in, &header)) {
        return READ_INVALID_HEADER;
    }

    *img = image_init(header.biWidth, header.biHeight);
    if (!img->data) {                                     // если память не выделилась, то ошибка
        return READ_INVALID_SIGNATURE;
    }

    if (!read_pixels(in, img)) {
        return READ_INVALID_BITS;
    }

    return READ_OK;
}

static uint32_t calculate_image_size (struct image const img) {
    return (img.width * 3 + calculate_padding(img.width) ) * img.height;
}

static struct bmp_header get_hdr_from_img( struct image const img ) {
    struct bmp_header header = {0};
    header.bfType = BF_TYPE;
    header.bfReserved = BF_RESERVED;
    header.bOffBits = B_OFF_BITS;
    header.biSize = BI_SIZE;
    header.biWidth = img.width;
    header.biHeight = img.height;
    header.biPlanes = BI_PLANES;
    header.biBitCount = BI_BIT_COUNT;
    header.biCompression = BI_COMPRESSION;
    header.biSizeImage = calculate_image_size(img);
    header.bfileSize = header.biSizeImage + B_OFF_BITS;
    return header;
}

static bool write_header( FILE* out, const struct image* const img) {
    struct bmp_header header = get_hdr_from_img(*img);
    if( !fwrite( &header, sizeof(struct bmp_header), 1, out )) {    // записываем новый заголовок в результирующий файл
        return false;
    }
    return true;
}

static bool write_pixels(FILE* out, const struct image* const img) {
    const uint8_t alignment[4] = {0};   // массив пикселей для выравнивания строк (B = 0; G = 0; R = 0)
    for( uint64_t y = 0; y < img->height; y++) {
        if ( !fwrite(img->data + img->width * y, sizeof(struct pixel) * img->width, 1, out) ) { // аналогично чтению файла, записываем по 1 пикселю результирующий файл
            return false;
        }
        if (!fwrite(alignment, calculate_padding(img->width) , 1, out)) {
            return false;
        } // *padding* выравнивание строки до длины кратной 4
    }
    return true;
}

// запись результирующего изображения в файл
enum write_status to_bmp( FILE* out, const struct image * const img ) {
    if (!out || !img) {
        return WRITE_BAD_FILE;
    }
    if (!write_header(out, img)) {
        return WRITE_INVALID_HEADER;
    }
    
    if (!write_pixels(out, img)) {
        return WRITE_INVALID_BITS;
    }
	return WRITE_OK;
}
