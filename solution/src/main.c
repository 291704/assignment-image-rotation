#include <stdio.h>

#include "bmp.h"
#include "file.h"
#include "image.h"
#include "rotation.h"

#define READ_BINARY "rb"
#define WRITE_BINARY "wb"
#define STATUS_OK 0
#define STATUS_ERROR 1

int main( int argc, char** argv ) {
    if (argc != 3) {
        fprintf(stderr, "Usage: ./image-transformer <source-image> <transformed-image>\n");   
        /* проверка на корректный запуск программы. То есть должны быть указаны файл, 
        который нужно прочитать и преобразовать, и файл, в который нужно записать*/
        return STATUS_ERROR;
    }

    FILE* in; FILE* out;
    if (!file_open(&in, argv[1], READ_BINARY)) {
        fprintf(stderr, "Error: can't open input file\n");
        return STATUS_ERROR;
    }

    struct image src_image;                                   // создаём структуру для исходного изображения.
    if (from_bmp(in, &src_image)) {
        fprintf (stderr, "Error: can't read bmp\n");
        file_close(in);
        return STATUS_ERROR;
    }

    struct image dst_image;
    if(!img_rotate(&src_image, &dst_image)) {
        fprintf (stderr, "Error: can't rotate image\n");
        image_destroy(&src_image);
        file_close(in);
        return STATUS_ERROR;
    }

    if (!file_open(&out, argv[2], WRITE_BINARY)) {
        fprintf (stderr, "Error: can't open output file\n");
        image_destroy(&src_image);
        image_destroy(&dst_image);
        file_close(in);
        return STATUS_ERROR;
    }
    if (to_bmp(out, &dst_image)) {
        fprintf (stderr, "Error: can't write bmp\n");
        image_destroy(&src_image);
        image_destroy(&dst_image);
        file_close(in);
        file_close(out);
        return STATUS_ERROR;
    }

    image_destroy(&src_image);
    image_destroy(&dst_image);
    file_close(in);
    file_close(out);
    return STATUS_OK;
}
